# Atelier_inference_réseaux


## install.packages

Finalement on n'utilisera pas `renv`

```r
install.packages("tidyverse")

install.packages("BiocManager")
BiocManager::install("phyloseq")

remotes::install_github("mahendra-mariadassou/phyloseq-extended", ref = "dev")

remotes::install_github("stefpeschel/NetCoMi",
                        ref = "develop",
                        dependencies = c("Depends", "Imports", "LinkingTo"),
                        repos = c("https://cloud.r-project.org/",
                                  BiocManager::repositories()))
```
